package com.daniel.authservice.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Login {
    @Value("${server.port}")
    private int porta;

    @GetMapping
    @RequestMapping("/login/{user}/{password}")
    public String getFoosBySimplePathWithPathVariable(
            @PathVariable("user") String user, @PathVariable("password") String password) {
        if (user != null && user.equals("user") && password != null && password.equals("user") ){

            //Loop para travar a requisição
            long i = 0;
            while(i < 10000000000L){
                i++;
            }
            return "Login realizado com sucesso! (AUTH-SERVICE: "+porta+")";
        }
        return "Senha inválida! Por favor tente novamente! (AUTH-SERVICE: "+porta+")";
    }
}
