package com.daniel.authservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurityApplication3 {

	public static void main(String[] args) {
		SpringApplication.run(SecurityApplication3.class, args);
	}

}
