package com.daniel.authservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SecurityApplication2 {

	public static void main(String[] args) {
		SpringApplication.run(SecurityApplication2.class, args);
	}

}
